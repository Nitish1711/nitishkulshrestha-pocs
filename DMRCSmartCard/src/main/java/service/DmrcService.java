package service;

import com.entities.Subscriber;

public interface DmrcService {

	public Subscriber registerUser(Subscriber user);
}
