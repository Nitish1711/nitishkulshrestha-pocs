package daoImpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.entities.Subscriber;

import dao.DmrcDao;

@Repository
public class DmrcDaoImpl implements DmrcDao{

	
	private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }

	
    public Subscriber registerUser(Subscriber user){
		 Session session = this.sessionFactory.getCurrentSession();
	        session.persist(user);
	        System.out.println("Subscriber saved successfully, Subscriber Details="+user);
		return user;
	}
}
