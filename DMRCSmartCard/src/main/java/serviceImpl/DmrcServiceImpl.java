package serviceImpl;

import org.springframework.stereotype.Service;

import com.entities.Subscriber;

import dao.DmrcDao;
import service.DmrcService;

@Service
public class DmrcServiceImpl implements DmrcService {

	private DmrcDao dmrcDao;
 	
	 public void setDmrcDAO(DmrcDao dmrcDAO) {
	        this.dmrcDao = dmrcDAO;
	    }
	
	public Subscriber registerUser(Subscriber user){
		return dmrcDao.registerUser(user);
	}


}
