package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.entities.Subscriber;

import service.DmrcService;

@Controller
public class DmrcController {
     
    private DmrcService dmrcService;
     
    @Autowired(required=true)
    @Qualifier(value="dmrcService")
    public void setPersonService(DmrcService ps){
        this.dmrcService = ps;
    }
     
    
     
    //For add and update person both
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public Subscriber addPerson(@ModelAttribute("subscriber") Subscriber sub){
 
    	 sub.setCardNumber("123455");
    	 sub.setName("Nitish");
    	  
 
    	
    	
       return dmrcService.registerUser(sub);
         
    }
     
  
     
}